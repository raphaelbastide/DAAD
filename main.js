
var express = require('express');
var fs = require('fs');
var path = require('path');
var stats = require('stats');
var pug = require('pug');
var app = express();

// Get the config
var config = require('config');
var serverConfig = config.get("server");
var project = config.get("project");

// Use pug as the view engine
app.set('view engine','pug');
app.use(express.static('public'));
app.get("/", getIndex);

var jsonData = new Object();
var projects = [];
jsonData.projects = projects;

mdSeek('public/'+project.dirname+'/','.md');

function getIndex(req, res) {
  var dataToSend = {
    title: "Titre de la page",
    data: jsonData
  }
  console.log(dataToSend);
  res.render("index", dataToSend);
};

function mdSeek(startPath,filter){
  if (!fs.existsSync(startPath)){
    console.log("no .md");
    return;
  }
  var files=fs.readdirSync(startPath);
  for(var i=0;i<files.length;i++){
    var filename=path.join(startPath,files[i]);
    var stat = fs.lstatSync(filename);
    if (stat.isDirectory()){
      mdSeek(filename,filter);
    }
    else if (filename.indexOf(filter)>=0) {
      var slug = filename.split(path.sep)[2];
      var folder = path.dirname(filename);
      var images = [];
      var project = {
        'slug' : slug,
        'folder' : folder,
        'images' : imgSeek(folder)
      }
      // projects[slug] = project;
      jsonData.projects.push(project);
    };
  };
  return jsonData = jsonData;
};
console.log(jsonData);

function imgSeek(dir){
  var imglist = [];
  var files=fs.readdirSync(dir);
  for(var i=0;i<files.length;i++){
    var filename=path.join(dir,files[i]);
    var stat = fs.lstatSync(filename);
    if (stat.isDirectory()){
      return
    }
    else if (filename.match(/.(jpg|jpeg|png|gif)$/i)) {
      filename = filename.split(path.sep)[3];
      imglist.push(filename);
    };
  };
  return imglist;
}

var server = app.listen(serverConfig.port, serverConfig.host, function () {
   var host = server.address().address;
   var port = server.address().port;
   console.log('DAAD listening at http://%s:%s', host, port);
});
